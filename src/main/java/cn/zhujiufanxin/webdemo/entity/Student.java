package cn.zhujiufanxin.webdemo.entity;

import lombok.Data;

/**
 * @author qsy
 * @date 2021年07月22日 21:40
 */
@Data
public class Student {

    private Long id;

    private String name;

    private Integer age;
}
