package cn.zhujiufanxin.webdemo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author qsy
 * @date 2021年07月09日 23:26
 */
@RestController
public class DemoController {

    @Value("${server.port}")
    private String port;

    @GetMapping("/getport")
    public String getPort(){
        return port;
    }
}
