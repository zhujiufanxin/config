package cn.zhujiufanxin.webdemo;

import cn.zhujiufanxin.webdemo.entity.Student;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class WebDemoApplicationTests {

    @Test
    void contextLoads() {
        Student student = new Student();
        student.setId(4L);
        student.setName("狂三");
        student.setAge(18);
        System.out.println(student.getId().equals(4));
        System.out.println(student.getId().equals(4L));
    }

}
